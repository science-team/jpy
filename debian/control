Source: jpy
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-sequence-python3,
 default-jdk,
 python3-all-dev,
 chrpath,
 python3-setuptools
Standards-Version: 4.7.0
Homepage: https://github.com/jpy-consortium/jpy
Vcs-Browser: https://salsa.debian.org/python-team/packages/jpy
Vcs-Git: https://salsa.debian.org/python-team/packages/jpy.git
Rules-Requires-Root: no

Package: python3-jpy
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}, default-jre | java7-runtime-headless
Description: Bi-directional Python-Java bridge (Python3)
 jpy is a **bi-directional** Python-Java bridge which you can use to embed
 Java code in Python programs or the other way round.
 It has been designed particularly with regard to maximum
 data transfer speed between the two languages.
 It comes with a number of outstanding features:
 .
  * Fully translates Java class hierarchies to Python
  * Transparently handles Java method overloading
  * Support of Java multi-threading
  * Fast and memory-efficient support of primitive Java array parameters via
    `Python buffers` (http://docs.python.org/3.3/c-api/buffer.html)
  * Support of Java methods that modify primitive Java array parameters
    (mutable parameters)
  * Java arrays translate into Python sequence objects
  * Java API for accessing Python objects (``jpy.jar``)
 .
 This package provides the Python3 interface.
